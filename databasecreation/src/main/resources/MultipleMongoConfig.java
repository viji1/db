///*
// * @Configuration public class MultipleMongoConfig {
// * 
// * @Primary
// * 
// * @Bean(name = "primary")
// * 
// * @ConfigurationProperties(prefix = "spring.data.mongodb") public
// * MongoProperties getPrimary() { return new MongoProperties(); }
// * 
// * @Secondary
// * 
// * @Bean(name = "secondary")

 @ConfigurationProperties(prefix = "mongodb")
 public MongoProperties getSecondary() {
	 return new MongoProperties(); }

 @Primary

 @Bean(name = "primaryMongoTemplate")
 public MongoTemplate primaryMongoTemplate() throws Exception { return new
 MongoTemplate(primaryFactory(getPrimary())); }
 
 @Secondary

 @Bean(name = "secondaryMongoTemplate") public MongoTemplate
 secondaryMongoTemplate() throws Exception { return new
 MongoTemplate(secondaryFactory(getSecondary())); }

@Bean
 
 @Primary public MongoDbFactory primaryFactory(final MongoProperties mongo)
 throws Exception { return new SimpleMongoDbFactory(new
 MongoClient(mongo.getHost(), mongo.getPort()), mongo.getDatabase()); }
 
 @Bean
 
 @Secondary public MongoDbFactory secondaryFactory(final MongoProperties
 mongo) throws Exception { return new SimpleMongoDbFactory(new
 MongoClient(mongo.getHost(), mongo.getPort()), mongo.getDatabase()); }
