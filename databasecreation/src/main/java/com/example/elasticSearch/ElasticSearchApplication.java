package com.example.elasticSearch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.elasticSearch.app.entity.PrimaryModel;



@SpringBootApplication
@RestController
public class ElasticSearchApplication{
	 @Autowired
	 MongoTemplate mongoTemplate;
public static void main(String[] args) {
	SpringApplication.run(ElasticSearchApplication.class, args);        
    }

	 @PostMapping(value = "/saveValue", produces = "application/json")
	public String save(PrimaryModel user) {
	     mongoTemplate.save(user, "secondary");
	     mongoTemplate.save(user, "primary");
		return mongoTemplate.toString();
	 }
	 
	   
   }

