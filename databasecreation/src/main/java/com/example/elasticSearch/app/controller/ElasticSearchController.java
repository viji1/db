/*
 * package com.example.elasticSearch.app.controller;
 * 
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.data.mongodb.core.MongoTemplate; import
 * org.springframework.http.HttpStatus; import
 * org.springframework.http.ResponseEntity; import
 * org.springframework.web.bind.annotation.CrossOrigin; import
 * org.springframework.web.bind.annotation.PostMapping; import
 * org.springframework.web.bind.annotation.RequestBody; import
 * org.springframework.web.bind.annotation.RestController;
 * 
 * import com.example.elasticSearch.app.entity.PrimaryModel; import
 * com.example.elasticSearch.app.entity.SecondaryModel; import
 * com.example.elasticSearch.app.repository.PrimaryRepository; import
 * com.example.elasticSearch.app.repository.SecondaryRepository;
 * 
 * @RestController
 * 
 * @CrossOrigin public class ElasticSearchController {
 * 
 * @Autowired PrimaryRepository primaryModel;
 * 
 * @Autowired SecondaryRepository secondaryModel;
 * 
 * @PostMapping(value = "/addValue", produces = "application/json") public
 * ResponseEntity<?> saveProfile(@RequestBody PrimaryModel profileSetup) throws
 * Exception {
 * 
 * 
 * // List<PrimaryModel> primaries = this.primaryModel.findAll(); // for
 * (PrimaryModel primary : primaries) { //
 * System.out.println(primary.toString()); // } // // List<SecondaryModel>
 * secondaries = this.secondaryModel.findAll(); // for (SecondaryModel secondary
 * : secondaries) { // System.out.println(secondary.toString()); // }
 * if(profileSetup.getWorkspaceType()=="Organization") { PrimaryModel
 * model=this.primaryModel.save(new PrimaryModel(profileSetup));
 * 
 * return new ResponseEntity<>(model, HttpStatus.OK);
 * 
 * } else { SecondaryModel modelData=this.secondaryModel.save(new
 * SecondaryModel(profileSetup)); return new ResponseEntity<>(modelData,
 * HttpStatus.OK); } }
 * 
 * }
 */