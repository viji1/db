package com.example.elasticSearch.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.elasticSearch.app.entity.PrimaryModel;

public interface PrimaryRepository extends MongoRepository<PrimaryModel, String> {
	
//	public PrimaryModel save(PrimaryModel primaryModel);

}
