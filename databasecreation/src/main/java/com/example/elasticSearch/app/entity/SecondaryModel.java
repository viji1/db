
package com.example.elasticSearch.app.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "secondary")
public class SecondaryModel {

	@Id
	public String id;

	public SecondaryModel() { 
		super(); // TODO Auto-generated constructor stub }
	}

	public SecondaryModel(String id, String fullName, String workspaceType, String teamWork) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.workspaceType = workspaceType;
		this.teamWork = teamWork;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getWorkspaceType() {
		return workspaceType;
	}

	public void setWorkspaceType(String workspaceType) {
		this.workspaceType = workspaceType;
	}

	public String getTeamWork() {
		return teamWork;
	}

	public void setTeamWork(String teamWork) {
		this.teamWork = teamWork;
	}

	public String fullName;

	public String workspaceType;
	public String teamWork;

}
