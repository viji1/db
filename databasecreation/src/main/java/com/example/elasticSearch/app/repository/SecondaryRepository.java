package com.example.elasticSearch.app.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.elasticSearch.app.entity.SecondaryModel;

public interface SecondaryRepository extends MongoRepository<SecondaryModel, String> {

}
